class User < ApplicationRecord
  has_secure_password

  validates :firstname, presence: true, blank: false, on: :update
  validates :lastname,  presence: true, blank: false, on: :update
end
